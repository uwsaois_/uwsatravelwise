<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/>

    <?php wp_head(); ?>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900|Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>
    <div id="wrap"><!-- This keeps the footer at the bottom of the page -->
        <header id="masthead" class="site-header" role="banner">
            <img class="visible-print-inline print-logo"
                 src="<?php echo get_template_directory_uri(); ?>images/UWsystem-print-logo.png" alt="University of Wisconsin System">

            <div class="systemRed hidden-print header-wrap">  <!-- adds a min-height for logo -->
                <div class="deepRedTop hidden-print"> <!-- gray bar, with min-height applied -->
                    <div class="container no-padding-bottom swish-container">
                        <div class="swish"></div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3 col-logo">
                                <a href="http://wisconsin.edu" class="logo-link"><img src="<?php echo get_template_directory_uri(); ?>/images/UWsystem-logo.svg" alt="University of Wisconsin System" onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/UWsystem-logo.png'; this.onerror=null;"></a>
                            </div>
                            <div class="col-xs-5 col-sm-7 col-md-8 pull-right">
                                <div class="globe-nav">
<!-- UWSA calls this the global menu in the UI toolkit. In this theme it is called the Top Red Search Menu. It contains javascript for the dropdown search-->
                                    <?php
                                    $top_red_menu_copy = wp_nav_menu(array(
                                    'theme_location' => 'top-red',
                                    'container_class' => 'global-menu',
                                    'menu_class' => 'list-unstyled list-inline',
                                    'menu_id' => 'global',
                                    'fallback_cb' => '',
                                    'depth' => 1,
                                    'echo' => false,
                                    'walker' => new wp_bootstrap_navwalker()
                                    ));
                                    ?>

                                    <?php if ($top_red_menu_copy ): ?>
                                        <?= $top_red_menu_copy; ?>
                                    <?php endif; ?>
                                </div>

                                <!-- Search Box -- currently hooked into WP basic search functionality -->

                                <?php if ( !$top_red_menu_copy ): ?>
                                    <div class="search-reticule">
                                        <a href="javascript:void(0);" class="start-search-link"><i class="uw-search"></i></a>
                                    </div>
                                <?php endif; ?>

                                <div class="red-search-form">
                                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                        <label>
                                            <span class="screen-reader-text sr-only">Search for:</span>
                                            <input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', '_tk' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', '_tk' ); ?>">
                                        </label>
                                        <div class="actions">
                                            <button type="submit" class="search-submit btn btn-default" value="<?php echo esc_attr_x( 'Search', 'submit button', '_tk' ); ?>"><i class="zuse-right-big"></i></button>
                                            <a href="javascript:void(0);" class="cancel-link"><i class="zuse-cancel"></i></a>
                                        </div>
                                    </form>


                                </div>


                                <!-- .navbar -->

                            </div>
                        </div>
                    </div>
                    <!-- .container -->
                </div>
                <!-- dark muted red bar, with min-height applied -->
                <div class="offRed hidden-print threequarter-em"></div>
                <!-- deep dark red bar, with min-height applied -->
            </div>
            <!-- .header-wrap -->
        </header>
        <!-- #masthead -->

        <nav class="site-navigation systemRed">
            <div class="container">


                <div class="row">
                    <div class="site-info col-md-6 col-md-offset-6">
                        <h2 class="site-title"> <a href="<?php echo esc_url(home_url('/')); ?>"
                           title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                           rel="home"><?php bloginfo('name'); ?></a></h2>
                        <h3 class="sub-title"><a href="#"><?php bloginfo( 'description' ); ?></a></h3>



                    </div>

                </div>



                <div class="row">
                    <div class="site-navigation-inner col-sm-12">
                        <div id="system-menu" class="navbar navbar-default">
                            <div class="navbar-header">
                                <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".uwsa-main-menu">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>



                            </div>
                            <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'primary',
                                    'container_class' => 'collapse navbar-collapse navbar-responsive-collapse uwsa-main-menu caps',
                                    'menu_class' => 'nav navbar-nav',
                                    'fallback_cb' => '',
                                    'menu_id' => 'main-menu',
                                    'depth' => 2,
                                    'walker' => new wp_bootstrap_navwalker()
                                )
                            ); ?>


                        </div>
                        <!-- .navbar -->
                    </div>
                </div>
            </div>
            <!-- .container -->
        </nav>
        <!-- .site-navigation -->


        <!-- Breadcrumbs -->
        <!-- You will need to add functionality here based on how your site is set up to build breadcrumbs. UWSA functionality is very specific to our site. -->

        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="http://www.wisconsin.edu/">University of Wisconsin System</a></li>
                    <li><a href="index.html">UI Toolkit</a></li>
                </ol>
            </div>
        </div>
        <!-- Breadcrumbs end -->
<div class="main-content">	
	<div class="container">
		<div class="row">
			<div class="main-content-inner col-sm-12 col-md-9">

