( function ($) {
    'use strict';
    $(document).ready( function() {

        //
        // Initializes bootstrap tooltip and popover
        //

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover();
        });

        //
        // Creates sidebar functionality for documentation
        //

        $('#sidebar').affix({
            offset: {
                top: function () {
                    return $('#leftCol').offset().top;
                },
                bottom: function () {
                    return (this.bottom = $('.site-footer').outerHeight(true));
                }
            }
        });

        var menuWidth = $('#leftCol').width();
        $('#leftCol li').css({width: menuWidth + 'px'});

        //
        // Use the same modal to handle both PRD and DEV downloads
        //

        $('#downloadModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var type = button.data('version'); // Extract info from data-* attributes
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('.modal-title').text('Download ' + type + ' Files');
            modal.find('.modal-body input.type').val(type);
        });




    });


    //
    // Creates the mobile view for the global menu
    //
    jQuery(function ($) {

        // Create the dropdown base
        $("<select />").appendTo(".global-menu");

        // Create default option "Go to..."
        $("<option />", {
            "selected": "selected",
            "value"   : "goto",
            "text"    : "Go to..."
        }).appendTo(".global-menu select");

        // Populate dropdown with menu items
        $(".global-menu a").each(function() {
            var el = $(this);
            $("<option />", {
                "value"   : el.attr("href"),
                "text"    : el.text()
            }).appendTo(".global-menu select");
        });
        $(".global-menu select").addClass("global-select");
        // To make dropdown actually work
        // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
        $(".global-menu select").change(function() {

            var val = $(this).find("option:selected").val();
            if ( val == "show-search" ) {
                show_search();
                $(this).val("goto");
            } else if ( val != "goto" ) {
                window.location = val;
            }
        });

        //setup search reticule
        $('<li class="menu-item"><a href="javascript:void(0);" class="start-search-link"><i class="uw-search"></i></a></li>').appendTo(".global-menu ul#global");
        $('<option value="show-search">Search</option>').appendTo(".global-menu select");

        $(".cancel-link").click(show_menu);
        $(".start-search-link").click(show_search);

        function show_search() {
            $(".search-reticule").animate({top: '-100px'}, 600, 'easeInOutBack');
            $(".globe-nav").animate({top: '-100px'}, 600, 'easeInOutBack');
            $(".red-search-form").animate({top: '0px'}, 800, 'easeInOutBack');
        }
        function show_menu() {
            $(".globe-nav").animate({top: '0px'}, 600, 'easeInOutBack');
            $(".search-reticule").animate({top: '0px'}, 600, 'easeInOutBack');
            $(".red-search-form").animate({top: '-100px'}, 800, 'easeInOutBack');
        }
    });




    //
    // Code for form submission
    //

    $(function(){
        //Get the form.
        var form=$('#downloadForm');

        //set up the event listener for the contact form.
        $(form).submit(function(event) {
            // stop the browser from submitting the form
            event.preventDefault();

            //confirm required fields are included
            var name = $('input#name').val();
            var email = $('input#email').val();
            var org = $('input#org').val();
            var altname = $('input#altname').val();
            var altemail = $('input#altemail').val();

            if (!(name && email && org && altname && altemail)) {
                writeAlert('All fields are required.','danger');
                //Don't submit form.
                return;
            }

            //Validate Email
            var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

            if (!(re.test(email) || re.test(altemail))) {
                writeAlert('Please submit a valid email.','danger');
                //Don't submit form.
                return;
            }

            // Serialize the form data.
            var formData = $(form).serialize();

            // Submit the form using AJAX.
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: formData,
                success: function() {
                    // Success message
                    writeAlert('Your message has been sent.','success');

                    //clear all fields
                    $('#downloadForm').trigger('reset');
                },
                error: function() {
                    // Fail message
                    writeAlert('Sorry, it seems that my mail server is not responding. Please try again later!','danger');
                    //clear all fields
                    $('#downloadForm').trigger('reset');
                }

            });
        });

    });

    function writeAlert(message,tone) {
        if (!tone) {tone='success';}

        $('#success').html('<div class="alert alert-' + tone + '">');

        var $noticeDiv = $('#success').find('.alert-' + tone);
        $noticeDiv.html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;');
        $noticeDiv.append('</button><strong>' + message + '</strong></div>');
    }


})(jQuery);