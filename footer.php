<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk customized for UWSA web team for Fox World Travel.
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

</div><!-- close #wrap -->

<footer class="site-footer" role="contentinfo">
	<div class="footer" >
		<div class="container">
			<div class="footer-wrapper">
				<div class="row">

					<div class="footer-col col-md-9 col-sm-6 col-xs-6 col-xxs-12 hidden-print">
						<div class="footer-content">
							<div class="col-md-4">
                                <!-- This is UWSA's global navigation. It is found on all pages of our site -->
								<h3 class="footer-link-heading">UW System Administration</h3>
								<ul class="list-unstyled footer-menu">
									<li ><a href="http://www.wisconsin.edu/about-the-uw-system/">What is the UW System</a></li>
									<li ><a href="http://www.wisconsin.edu/for-wisconsin/">For Wisconsin</a></li>
									<li ><a href="http://www.wisconsin.edu/news/">News</a></li>
									<li ><a href="http://www.wisconsin.edu/regents/">Regents</a></li>
									<li ><a href="http://www.wisconsin.edu/campuses/">Campuses</a></li>
									<li ><a href="http://www.wisconsin.edu/offices/">Offices</a></li>
									<li ><a href="https://www.wisconsin.edu/policies/">Policies</a></li>
									<li ><a href="http://www.wisconsin.edu/contact-the-uw-system/">Contact the UW System</a></li>
								</ul>
							</div>

							<div class="col-md-4 hidden-print">

								<h3 class="footer-link-heading">Fox World Travel</h3>
								<ul class="list-unstyled footer-menu uws-description">
									<li> Business Travel Center Central</li>
									<li >Hours: 7:00am-7:30pm</li>
									<li >Phone: 920-230-6467</li>
									<li >Toll-Free: 866-230-8787</li>
									<li >Fax: 920-236-8045</li>
									<li >Online Support: <a href="mailto:UWOnline@gofox.com">UWOnline@gofox.com</a></li>
									<li >Agent Support: <a href="mailto:UWAgents@gofox.com">UWAgents@gofox.com</a></li>
								</ul>
							</div>


							<div class="col-md-4 hidden-print">
								<h3 class="footer-link-heading">After Hours & Emergency Support</h3>
								<ul class="list-unstyled footer-menu uws-description">
									<li >Hours: 7:30pm-7:00</li>
									<li >Phone: 920-738-5808</li>
									<li >Toll-Free: 800-388-9
								</ul>
							</div>

						</div><!-- end .footer-content-->
					</div><!-- end .footer-col-->


					<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 footer-col">
						<div class="footer-content-logo">
							<div class="footer-logo hidden-print">
								<img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png" alt="University of Wisconsin System">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/fox-logo.png" alt="Fox World Travel">
                                </div>
						</div><!-- end .footer-content-logo-->
					</div><!-- end col-md-3 col-sm-6 col-xs-6 col-xxs-12 footer-col-->
				</div><!-- end .row-->
			</div><!-- close .footer-wrapper-->
		</div>
		<!-- close .container -->

		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 center-text">
						<div class="copyright-text">
							<p>Content developed in collaboration with fox world travel and</p><p class="space_10">UW System Travel Management & Operations Committee Travel Administrators.</p>
							<p>© 2015 Board of Regents - University of Wisconsin System. All Rights Reserved</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- close #footer  -->
</footer><!-- close .site-footer -->


<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<?php wp_footer(); ?>

</body>
</html>